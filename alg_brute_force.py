import sys
from time import process_time

from utils import *


def brute_init(planets):
    start_t = process_time()
    r = brute(planets, [0], 0, 0)
    end_t = process_time()

    return {
        'alg': 'BF',
        'size': len(planets)-1,
        'state': r['state'],
        'value': r['value'],
        'evaluations_history': [],
        'log': [],
        'time': end_t - start_t
    }


def brute(planets, visited, deuter_gained, deuter_lost):
    if len(visited) > 1 and visited[-1] == 0:
        return {
            'state': visited,
            'value': deuter_gained / deuter_lost
        }

    current = planets[visited[-1]]
    deuter_gained += current['deuter']
    best = {
        'state': [],
        'value': 0
    }
    for c in current['connections']:
        next = planets[c]
        distance = calc_planets_distance(current, next)
        new_visited = visited + [c]
        new_deuter_lost = deuter_lost + distance
        if deuter_gained >= deuter_lost and (c == 0 or c not in visited):
            res = brute(planets, new_visited, deuter_gained, new_deuter_lost)
            if res['value'] > best['value']:
                best = res
    return best


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Usage: python ./alg_brute_force.py path_to_data")
        exit(-1)

    planets = get_planets(sys.argv[1])

    brute_res = brute_init(planets)
    print("Brute force:")
    print(f"{brute_res['state']} with value of {brute_res['value']}")
