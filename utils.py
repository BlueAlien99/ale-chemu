import math
import random
import json


def get_seed():
    with open("./seed.txt", "r") as f:
        return f.read()


def get_planets(path):
    with open(path, "r") as f:
        planets = json.loads(f.read())
        return {int(k): v for k, v in planets.items()}


def calc_planets_distance(planet_a, planet_b):
    dsum = 0
    for c in range(len(planet_a)):
        dsum += (planet_a['coords'][c] - planet_b['coords'][c]) ** 2
    return math.sqrt(dsum)


def get_random_state(planets):
    path_lengths = list(range(1, len(planets)))
    while len(path_lengths) > 0:
        pool_idx = random.randint(0, len(path_lengths) - 1)
        choosed_path_len = path_lengths[pool_idx]
        path_lengths[pool_idx] = path_lengths[-1]
        path_lengths.pop()

        [found, state] = get_random_state_rec(planets, [], 0, choosed_path_len)
        if found:
            return state
    
    return None


def get_random_state_rec(planets, state, deuter, target_len):
    if len(state) == target_len:
        return calc_planets_distance(planets[0], planets[state[-1]]) <= deuter, state
    
    pool = []
    current_planet = planets[0]
    if len(state) == 0:
        deuter = current_planet['deuter']
    else:
        current_planet = planets[state[-1]]

    for planet_idx in current_planet['connections']:
        if planet_idx in state or planet_idx == 0:
            continue
        planet = planets[planet_idx]
        if calc_planets_distance(current_planet, planet) <= deuter:
            pool.append(planet_idx)
        
    while len(pool) > 0:
        choosed_planet_idx = random.randint(0, len(pool) - 1)
        next_planet_idx = pool[choosed_planet_idx]
        pool[choosed_planet_idx] = pool[-1]
        pool.pop()
        
        new_state = state.copy()
        new_state.append(next_planet_idx)

        distance = calc_planets_distance(current_planet, planets[next_planet_idx])
        new_deuter = deuter + planets[next_planet_idx]['deuter'] - distance

        [found, result] = get_random_state_rec(planets, new_state, new_deuter, target_len)
        if found:
            return found, result

    return False, None


def evaluate_state(state, planets):
    current_planet = planets[0]
    deuter_gained = current_planet['deuter']
    deuter_used = 0
    is_valid = True
    for idx in range(0, len(state)):
        planet_idx = state[idx]

        if planet_idx not in current_planet['connections']:
            return -1, False
        
        deuter_used += calc_planets_distance(current_planet, planets[planet_idx])
        if deuter_used > deuter_gained:
            is_valid = False

        current_planet = planets[planet_idx]
        deuter_gained += current_planet['deuter']

    deuter_used += calc_planets_distance(current_planet, planets[0])
    if deuter_used > deuter_gained:
        is_valid = False

    return (deuter_gained / deuter_used), is_valid


def validate_connections(state, planets):
    current_planet = planets[0]
    for planet_idx in state:
        if planet_idx not in current_planet['connections']:
            return False
        
        current_planet = planets[planet_idx]

    return True


def get_neighbours(state, planets):
    neighbours = []

    # transpositions
    for i in range(0, len(state)):
        for j in range(i + 1, len(state)):
            new_state = state.copy()
            temp = new_state[i]
            new_state[i] = new_state[j]
            new_state[j] = temp
            if validate_connections(new_state, planets):
                neighbours.append(new_state)

    # removal
    if len(state) > 1:
        for i in range(0, len(state)):
            new_state = state.copy()
            new_state.remove(state[i])
            if validate_connections(new_state, planets):
                neighbours.append(new_state)

    # insertion
    options = list(range(1, len(planets)))
    options = list(filter(lambda x: x not in state, options))
    for option in options:
        for i in range(0, len(state)):
            new_state = state[0:i] + [option] + state[i:]
            if validate_connections(new_state, planets):
                neighbours.append(new_state)

        new_state = state.copy()
        new_state.append(option)
        if validate_connections(new_state, planets):
            neighbours.append(new_state)

    return neighbours


def get_random_neighbour(state, planets):
    neighbours = get_neighbours(state, planets)
    return random.choice(neighbours)
