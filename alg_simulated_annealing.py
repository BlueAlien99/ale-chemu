import sys
from time import process_time

from utils import *
from plotters import *


def simulated_annealing(planets, max_iter, start_temp, temp_decay):
    random.seed(get_seed())
    start_t = process_time()

    state = get_random_state(planets)
    [value, _] = evaluate_state(state, planets)
    temperature = start_temp
    log = [{'state': state, 'value': value, 'temp': temperature}]
    state_evaulations = 1
    evaulations_count_history = [{'evaluations': state_evaulations, 'value': value}]

    best_state = state
    best_value = value

    for _ in range(0, max_iter):
        candidate = get_random_neighbour(state, planets)
        [candidate_value, is_valid] = evaluate_state(candidate, planets)
        state_evaulations += 1

        if candidate_value > value:
            state = candidate
            value = candidate_value

            if value > best_value and is_valid:
                best_value = value
                best_state = state
        else:
            acceptance_probability = math.exp(-abs(candidate_value - value)/temperature)
            random_value = random.random()
            if random_value < acceptance_probability:
                state = candidate
                value = candidate_value

        log.append({'state': state, 'value': value, 'temp': temperature})
        evaulations_count_history.append({'evaluations': state_evaulations, 'value': best_value})
        temperature *= temp_decay

    end_t = process_time()

    return {
        'alg': 'SA',
        'size': len(planets)-1,
        'state': best_state,
        'value': best_value,
        'evaluations_history': evaulations_count_history,
        'log': log,
        'time': end_t - start_t
     }


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python ./alg_simulated_annealing.py path_to_data MAX_ITERS [START_TEMP] [TEMP_DECAY]")
        exit(-1)

    planets = get_planets(sys.argv[1])
    max_iters = int(sys.argv[2])
    start_temp = 100 if len(sys.argv) < 4 else float(sys.argv[3])
    temp_decay = 0.95 if len(sys.argv) < 5 else float(sys.argv[4])

    result = simulated_annealing(planets, max_iters, start_temp, temp_decay)
    print(f"Simulated annealing with max. {max_iters} iterations, temp. {start_temp} and decay {temp_decay}:")
    print(f"{result['state']} with value of {result['value']}")

    plot_log(result)
    plot_temp(result)
