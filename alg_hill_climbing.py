import sys
from time import process_time

from utils import *
from plotters import *


def hill_climbing(planets, max_iters):
    random.seed(get_seed())
    start_t = process_time()

    state = get_random_state(planets)
    [value, _] = evaluate_state(state, planets)
    log = [{'state': state, 'value': value}]
    state_evaulations = 1
    evaulations_count_history = [{'evaluations': state_evaulations, 'value': value}]

    for _ in range(0, max_iters):
        neighbours = get_neighbours(state, planets)
        found_better = False

        for neighbour in neighbours:
            [candidate_value, is_valid] = evaluate_state(neighbour, planets)
            state_evaulations += 1
            if candidate_value > value and is_valid:
                value = candidate_value
                state = neighbour
                found_better = True
            evaulations_count_history.append({'evaluations': state_evaulations, 'value': value})

        if not found_better:
            break

        log.append({'state': state, 'value': value})

    end_t = process_time()

    return {
        'alg': 'HC',
        'size': len(planets)-1,
        'state': state,
        'value': value,
        'evaluations_history': evaulations_count_history,
        'log': log,
        'time': end_t - start_t
    }


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python ./alg_hill_climbing.py path_to_data MAX_ITERS")
        exit(-1)

    planets = get_planets(sys.argv[1])
    max_iters = int(sys.argv[2])

    result = hill_climbing(planets, max_iters)
    print(f"Hill climbing with max. {max_iters} iterations:")
    print(f"{result['state']} with value of {result['value']}")

    plot_log(result)
