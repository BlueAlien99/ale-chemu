import sys
import math
import random
import json
from functools import reduce

from utils import get_seed

import matplotlib.pyplot as plt


# random gauss
def rg():
    return random.gauss(0, 1)


def distance(coords):
    return math.sqrt(reduce(lambda acc, cur: acc + cur**2, coords, 0))

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("usage: python ./generator.py problem_size [connection_coef] [deuter_std_dev]")
        exit(-1)

    # foreign planets
    n = int(sys.argv[1])
    # all planets
    m = n+1

    # connection coefficient:
    # 0 = every planet is connected only to home planet
    # 1 = every planet is connected to each other
    CONN_COEF = 0.7 if len(sys.argv) < 3 else float(sys.argv[2])
    DEUTER_STD_DEV = 0.5 if len(sys.argv) < 4 else float(sys.argv[3])
    DIM = 3

    seed = get_seed()
    random.seed(seed)

    R = n ** (1/DIM)
    start_coords = tuple(0 for i in range(DIM))
    planets = {0: {
        'coords': start_coords,
        'deuter': R,
        'connections': []
    }}

    next_id = 1
    # generate planets
    # https://math.stackexchange.com/a/1585996
    for i in range(n):
        denormalized = tuple(rg() for _ in range(DIM))

        normalization = distance(denormalized)

        if normalization != 0:
            normalized = tuple(e / normalization for e in denormalized)
        else:
            normalized = denormalized

        r = R * random.triangular(0, 1, 1)
        coords = tuple(e * r for e in normalized)

        deuter = distance(coords) * max(0.0, random.gauss(1, DEUTER_STD_DEV))
        planets[next_id] = {'coords': coords, 'deuter': deuter, 'connections': []}
        next_id += 1

    # generate connections
    connections = []
    for i in range(1, n+1):
        for j in range(i+1, n+1):
            connections.append((i, j))

    conn_num = round(n * ((m/2) ** CONN_COEF))
    conn_to_rm = len(connections) - (conn_num - n)
    for i in range(conn_to_rm):
        idx = random.randrange(len(connections))
        del connections[idx]

    for i in range(1, n+1):
        connections.append((0, i))

    for c in connections:
        planets[c[0]]['connections'].append(c[1])
        planets[c[1]]['connections'].append(c[0])

    # save problem data to file
    problem_id = f"{seed}_{DIM}d_{n}"
    with open(f"./problems/{problem_id}.json", "w") as f:
        f.write(json.dumps(planets, indent=4))

    # only plots
    if DIM == 2 or DIM == 3:
        # prepare data for scatter plot
        coords = [[] for i in range(DIM)]
        deuter = []
        for p in planets.values():
            for i, coord in enumerate(p['coords']):
                coords[i].append(coord)
            deuter.append(p['deuter'])

        # plot padding
        Rv = R * 1.1

        fig = plt.figure(figsize=(12, 12))

        if DIM == 2:
            ax = fig.add_subplot()
            ax.scatter(coords[0], coords[1], c=deuter)
            ax.set_xlim(-Rv, Rv)
            ax.set_ylim(-Rv, Rv)
        elif DIM == 3:
            ax = fig.add_subplot(projection="3d")
            ax.scatter(coords[0], coords[1], coords[2], c=deuter)
            ax.set_xlim3d(-Rv, Rv)
            ax.set_ylim3d(-Rv, Rv)
            ax.set_zlim3d(-Rv, Rv)
        else:
            raise Exception("Impossible!")

        for c in connections:
            ca = planets[c[0]]['coords']
            cb = planets[c[1]]['coords']

            if DIM == 2:
                x = [ca[0], cb[0]]
                y = [ca[1], cb[1]]
                plt.plot(x, y, 'r-', linewidth=0.2)
            elif DIM == 3:
                x = [ca[0], cb[0]]
                y = [ca[1], cb[1]]
                z = [ca[2], cb[2]]
                plt.plot(x, y, z, 'r-', linewidth=0.2)
            else:
                raise Exception("Impossible!")

        plt.savefig(f"./problems/{problem_id}.png")
        plt.show()
