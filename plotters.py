import random
import time

import matplotlib.pyplot as plt
import numpy as np


SAVE_DIR = './plots'

COLORS = {
    'SA': 'tab:orange',
    'HC': 'tab:green',
    'TS': 'tab:blue',
}


def get_plotname():
    return f"{SAVE_DIR}/{time.time()}-{random.randint(100, 999)}.png"


def plot_brute_times(solutions):
    xs = [x['size'] for x in solutions]
    ys = [x['time'] for x in solutions]

    plt.title("Brute force")
    plt.xlabel("Problem size")
    plt.ylabel("Time [s]")

    plt.plot(xs, ys)
    plt.savefig(get_plotname())
    plt.show()


def plot_evaluations(solutions, baseline = None):
    plotted = set()
    for s in solutions:
        if s['size'] not in plotted and s['alg'] != 'BF':
            size = s['size']
            plotted.add(size)
            to_plot = [x for x in solutions if x['size'] == size and x['alg'] != 'BF']

            for tp in to_plot:
                xs = [x['evaluations'] for x in tp['evaluations_history']]
                ys = [x['value'] for x in tp['evaluations_history']]
                color = COLORS[tp['alg']]
                plt.plot(xs, ys, color, label=tp['alg'])
                plt.plot(xs[-1], ys[-1], color=color, marker='x')

            if baseline is not None:
                baseline_value = None
                for baseline_solution in baseline:
                    if baseline_solution['size'] == size:
                        baseline_value = baseline_solution['value']

                horiz_line_data = np.array([baseline_value for _ in range(len(xs))])
                plt.plot(xs, horiz_line_data, 'r--', label='baseline') 

            plt.title(f"Comparison of different algorithms n={size}")
            plt.xlabel("Evaluation number")
            plt.ylabel("Objective function value")
            plt.ylim(bottom=1.0)
            plt.legend()

            plt.savefig(get_plotname())
            plt.show()


def plot_log(result):
    alg_name = result['alg']
    log = result['log']

    xs = [x for x in range(len(log))]
    ys = [x['value'] for x in log]

    color = COLORS[alg_name]
    plt.plot(xs, ys, color=color, label=alg_name)

    plt.title("Objective function value as algorithm works")
    plt.xlabel("Index in log")
    plt.ylabel("Objective function value")
    plt.legend()

    plt.savefig(get_plotname())
    plt.show()

def plot_temp(result):
    log = result['log']

    xs = [x for x in range(len(log))]
    ys = [x['temp'] for x in log]

    plt.plot(xs, ys)

    plt.title("Temperature as algorithm works")
    plt.xlabel("Algorithm iteration")
    plt.ylabel("Temperature")

    plt.savefig(get_plotname())
    plt.show()
