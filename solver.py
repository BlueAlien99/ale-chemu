from functools import reduce
from os import listdir
from os.path import join
import sys
from time import sleep
from concurrent.futures import ProcessPoolExecutor

from utils import *
from plotters import *

from alg_brute_force import brute_init
from alg_hill_climbing import hill_climbing
from alg_simulated_annealing import simulated_annealing
from alg_tabu_search import tabu_search


def run_brute(path):
    planets = get_planets(path)
    return brute_init(planets)


def run_hc(path):
    planets = get_planets(path)
    MAX_ITERS = 100
    return hill_climbing(planets, MAX_ITERS)


def run_sa(path):
    planets = get_planets(path)
    MAX_ITERS = 10000
    TEMP = 100
    DECAY = 0.95
    return simulated_annealing(planets, MAX_ITERS, TEMP, DECAY)


def run_ts(path):
    planets = get_planets(path)
    MAX_ITERS = 100
    TABU_SIZE = 35
    return tabu_search(planets, MAX_ITERS, TABU_SIZE)


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python ./solver.py dir_with_data MAX_CPUS MODE")
        print("MODE:")
        print("0 - brute")
        print("1 - heuristics")
        print("2 - heuristics + brute")
        exit(-1)

    # find path to every problem to solve
    problems_dir = sys.argv[1]
    problems = [join(problems_dir, f) for f in listdir(problems_dir) if f[-5:] == ".json"]
    solutions = []

    mode = int(sys.argv[3])
    baseline = []
    baseline_solutions = []

    brute = True if mode == 0 else False
    algs = [run_brute] if brute else [run_hc, run_ts, run_sa]

    max_cpus = int(sys.argv[2])
    results = []

    executor = ProcessPoolExecutor(max_cpus)

    if mode == 2:
        print("Acquiring baseline results...")
        for p in problems:
            result = executor.submit(run_brute, p)
            baseline.append(result)

        is_running = True
        total_jobs = len(problems)
        while is_running:
            finished_jobs = reduce(lambda acc, t: acc + (1 if t.done() else 0), baseline, 0)
            running_jobs = min(max_cpus, total_jobs - finished_jobs)
            if finished_jobs == total_jobs:
                is_running = False
            print(f"Finished {finished_jobs} / {total_jobs} -> Running {running_jobs}")
            sleep(1)

    # start thread for every problem
    for p in problems:
        for a in algs:
            result = executor.submit(a, p)
            results.append(result)

    total_jobs = len(algs) * len(problems)

    # monitor running threads
    is_running = True
    while is_running:
        finished_jobs = reduce(lambda acc, t: acc + (1 if t.done() else 0), results, 0)
        running_jobs = min(max_cpus, total_jobs - finished_jobs)
        if finished_jobs == total_jobs:
            is_running = False
        print(f"Finished {finished_jobs} / {total_jobs} -> Running {running_jobs}")
        sleep(1)

    for result in results:
        solutions.append(result.result())

    for baseline_result in baseline:
        solutions.append(baseline_result.result())
        baseline_solutions.append(baseline_result.result())

    # pretty print solutions
    solutions = sorted(solutions, key=lambda e: e['size'])
    for s in solutions:
        print(f"{s['alg']} for {s['size']: <2} planets in {format(s['time'], '.2f')+'s': <6}: "
              f"{str(s['state']): <24} with value of  {format(s['value'], '.2f')}")

    if brute:
        plot_brute_times(solutions)
    elif mode == 1:
        plot_evaluations(solutions)
    else:
        baseline_solutions = sorted(baseline_solutions, key=lambda e: e['size'])
        plot_brute_times(baseline_solutions)
        plot_evaluations(solutions, baseline_solutions)
