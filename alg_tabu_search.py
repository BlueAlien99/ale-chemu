import sys
from time import process_time

from utils import *
from plotters import *


def tabu_search(planets, max_iter, tabu_size):
    random.seed(get_seed())
    start_t = process_time()

    state = get_random_state(planets)
    [value, is_valid] = evaluate_state(state, planets)
    tabu = []
    log = [{'state': state, 'value': value}]
    state_evaulations = 1
    evaulations_count_history = [{'evaluations': state_evaulations, 'value': value}]

    best_state = state
    best_value = value

    for _ in range(0, max_iter):
        neighbours = get_neighbours(state, planets)
        for t in tabu:
            if t in neighbours:
                neighbours.remove(t)

        value = -1
        for candidate in neighbours:
            [candidate_value, is_valid] = evaluate_state(candidate, planets)
            state_evaulations += 1
            if candidate_value > value:
                state = candidate
                value = candidate_value

            if value > best_value and is_valid:
                best_value = value
                best_state = state

            evaulations_count_history.append({'evaluations': state_evaulations, 'value': best_value})

        if len(tabu) == tabu_size:
            tabu.pop()

        log.append({'state': state, 'value': value})

        tabu.append(state)

    end_t = process_time()

    return {
        'alg': 'TS',
        'size': len(planets)-1,
        'state': best_state,
        'value': best_value,
        'evaluations_history': evaulations_count_history,
        'log': log,
        'time': end_t - start_t
     }


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python ./alg_tabu_search.py path_to_data MAX_ITERS [TABU_SIZE]")
        exit(-1)

    planets = get_planets(sys.argv[1])
    max_iters = int(sys.argv[2])
    tabu_size = 20 if len(sys.argv) < 4 else sys.argv[3]

    result = tabu_search(planets, max_iters, tabu_size)
    print(f"Tabu search with max. {max_iters} iterations and tabu size {tabu_size}:")
    print(f"{result['state']} with value of {result['value']}")

    plot_log(result)
